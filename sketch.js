var Xspeed = 3;
var Yspeed = 3;
var color = 50;
var ran = 4;
var x = 50;
var y = 50;
var Yn = .04;
var Xn = .04;
function setup() {
  createCanvas(600,400);
}
function draw() {
  background(0);
  stroke(255,255,255);
  strokeWeight(ran);
  noFill();
  fill(color,color,color);
  ellipse(x, y, 100, 100);

  if (mouseIsPressed) {
    x = mouseX;
    y = mouseY;
  }
  else {
    if (Yspeed > .02 || Yspeed < -.02){
      Yspeed = Yspeed - Yn;
    } else {
      Yspeed = 0;
    }

    if (Xspeed > .02 || Xspeed < -.02){
      Xspeed = Xspeed - Xn;
    } else {
      Xspeed = 0;
    }

    // console.log('Xspeed: ' + Xspeed + ' / Yspeed: ' + Yspeed + ' / x: ' + x + ' / y: ' + y);
    // console.log('pmouseX: ' + (pmouseX - mouseX) + ' / pmouseY: ' + (pmouseY - mouseY));

    if (x > 550 || x < 50) {
      Xspeed = -Xspeed;
      Xn = -Xn
      color = random(0,255);
      ran = random(0,10);
    }

    if (y > 350 || y < 50) {
      Yspeed = -Yspeed;
      Yn = -Yn
      color = random(0,255);
      ran = random(0,10);
    }

    y = y + Yspeed;
    x = x + Xspeed;
  }
}

function keyPressed() {
  if (keyCode === ENTER) {
    Yspeed = 0;
    Xspeed = 0;
    return;
}

}

  function mouseReleased() {

  if (Yn < 0){
      Yspeed = -(pmouseY - mouseY);
        console.log('up');
  } else {
    Yspeed = (pmouseY - mouseY);
      console.log('down');
  }
  if (Xn < 0){
    Xspeed = -(pmouseX - mouseX);
      // console.log('');
  } else {
    Xspeed = (pmouseX - mouseX);
      // console.log('4');
  }
}
